require 'spec_helper'

class TestApp < Rulers::Application
end

describe Rulers do
  include Rack::Test::Methods
  it 'has a version number' do
    expect(Rulers::VERSION).not_to be nil
  end

  it "get correct response" do
    get '/'

    expect(last_response.status).to eq(200)
    expect(last_response.body).to eq("get rack'd")
  end

  def app
    TestApp.new
  end
end
